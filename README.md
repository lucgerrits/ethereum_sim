

1) Get Shell to execute the scripts

./rancher kubectl exec -it -n ethereum-net $(./rancher kubectl -n ethereum-net get pods | awk '/ubuntu-/{printf $1}') -- bash -c ""

./rancher kubectl -n ethereum-net exec --stdin --tty $(./rancher kubectl -n ethereum-net get pods | awk '/ubuntu-/{printf $1}') -- /bin/bash

./rancher kubectl -n ethereum-net exec --stdin --tty $(./rancher kubectl -n ethereum-net get pods | awk '/ubuntu2-/{printf $1}') -- /bin/bash

2) Install VI editor

apt-get update

apt-get install vim git -y

3) Change /etc/hosts for redirecting to TAS

185.52.32.4 rancher.unice.cust.tasfrance.com
185.52.32.4 grafana.unice.cust.tasfrance.com
185.52.32.4 ethereum-ws.unice.cust.tasfrance.com
185.52.32.4 ethereum-http.unice.cust.tasfrance.com
185.52.32.4 ethmonitor.unice.cust.tasfrance.com
185.52.32.4 ethereum-ws1.unice.cust.tasfrance.com
185.52.32.4 ethereum-http1.unice.cust.tasfrance.com
185.52.32.4 ethereum-http-mongodb.unice.cust.tasfrance.com

4) Clone the repo 

git clone https://cyrilnavessamuel@bitbucket.org/lucgerrits/ethereum_sim.git

5) Install the dependencies on the Ubuntu VM for running solidity transactions

cd ethereum_sim/clients/clique/bootstrap

./createnenv.sh

6) For generating new accouts and funding them

 cd ethereum_sim/clients/clique/bootstrap/
 
 npm install .

 node genkeys.js

 7) Extract Private Key
 cd /ethereum_sim/clients/clique/utility

 python3 extractprivate.py

 8) Fund the account of cars with ethers

 cd ethereum_sim/clients/clique/bootstrap/
 python3 fundaccounts.py

 9) Deploy the smart contract main.sol

 cd ethereum_sim/clients/clique/deploy

 python3 deploy.py

 contractAddress=0xaec36f4f8D417a5e2987F26AA0a1831965c001CC


